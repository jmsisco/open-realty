# Open-Realty

**Description**: Open-Realty® is a web-based real estate listing management and lead generation content management system (CMS) that is designed to be very reliable and flexible as a framework for creating and managing a real estate website.

**Screenshot**:

![](https://gitlab.com/appsbytherealryanbonham/open-realty/-/raw/main/screenshot.png)

## Dependencies

- PHP v 8.0+
- MySQL 5.5 - 5.7
- PHP GD Support or Imagemagik
- PHP Multibyte String Support
- PHP CURL support
- PHP OpenSSL support
- PHP ZIP support
- PHP Short Tag support disabled
- PHP Safe Mode disabled
- PHP magic_quotes set to OFF (in all program folders)
- PHP session.auto_start disabled (session.auto_start = "0")
- PHP SuExec/SuPHP (recommended for automatic upgrades)
- APACHE mod_rewrite (required for SEO Friendly URLs)
- APACHE mod_expires (recommended)
- APACHE mod_headers (recommended)

## Installation

[Installation Guide](https://docs.open-realty.org/nav.guide/01.installation/)

## Running Open-Realty from source for development

Requirements: Docker Compose, Yarn, PHP 8.0+

1. Clone our gitlab repo or your [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html). _Forking is recommended, as you will need to commit your changes to your fork and open a merge request back to our repo in order to contribute your changes._

   `git clone git@gitlab.com:appsbytherealryanbonham/open-realty.git`

2. Change into the open-realty directory that was just created.

   `cd open-realty`

3. Start our docker-compose images.

   `make start-docker`

## Getting help

For general help, try our [discord server](https://discord.gg/uU7EYnxW).
If you have feature request or bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

See our [CONTRIBUTING](CONTRIBUTING.md) guide.

---

## Open source licensing info

[MIT LICENSE](LICENSE)
