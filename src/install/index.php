<?php
error_reporting(E_ALL ^ E_NOTICE);
// just so we know it is broken
session_start([
    "cookie_httponly" => true,
]);
require_once dirname(__FILE__).'/base_installer.php';
$installer = new installer();
$installer->show_header();
$installer->run_installer();
$installer->show_footer();
