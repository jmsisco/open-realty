<?php
/**
 * This File Contains the main API Framework
 *
 * @package Open-Realty
 * @subpackage API
 * @author Ryan C. Bonham
 * @copyright 2010
 * @link http://www.open-realty.com Open-Realty
 */


/**
 * This is the base API, all other API functions simply extend this class.
 *
 * @package Open-Realty
 * @subpackage API
 **/

define('BETA_API', false);

class api
{
    public function __construct()
    {
        global $lang,$lapi,$config;
        $lapi=$this;
        $this->load_setting();
    }

    /**
     * This function encrypts the string passed to it using the MCRYPT_RIJNDAEL_256 encrytion method.
     *
     * @param string $string This should contain the value to be encryted.
     * @return string Returns the encrypted version of the $string that was passed to the function.
     * @access private
     */
    public function RIJNDAEL256_encrypt($string)
    {
        global $config;
        // Encryption Algorithm
        $cipher_alg = MCRYPT_RIJNDAEL_256;
        // Create the initialization vector for added security.
        //$iv = mcrypt_create_iv(mcrypt_get_iv_size($cipher_alg, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $iv = substr(md5($config['apikey']), 0, mcrypt_get_iv_size($cipher_alg, MCRYPT_MODE_CBC));
        // Encrypt $string
        $encrypted_string = mcrypt_encrypt($cipher_alg, $config['apikey'], $string, MCRYPT_MODE_CBC, $iv);
        return $encrypted_string;
    }

    /**
     * This function decrypts the string passed to it using the MCRYPT_RIJNDAEL_256 decryption method.
     *
     * @param string $string This should contain the value to be decrypted.
     * @return string Returns the decrypted version of the encrypted $string that was passed to the function.
     * @access private
     */
    public function RIJNDAEL256_decrypt($string)
    {
        global $config;
        // Encryption Algorithm
        $cipher_alg = MCRYPT_RIJNDAEL_256;
        // Create the initialization vector for added security.
        //$iv = mcrypt_create_iv(mcrypt_get_iv_size($cipher_alg, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $iv = substr(md5($config['apikey']), 0, mcrypt_get_iv_size($cipher_alg, MCRYPT_MODE_CBC));
        $decrypted_string = mcrypt_decrypt($cipher_alg, $config['apikey'], $string, MCRYPT_MODE_CBC, $iv);
        return trim($decrypted_string);
    }

    /**
     * This builds the corerct xml response. It takes care of serialzing any arrays and encrypting the values
     * before sending them back to the client. All api commands should use this function when returning results.
     *
     * @param array $value
     * @return array
     * @access private
     */
    public function build_xml_response($value, $type)
    {
        //echo '<pre>Raw XMl Response: '.print_r($value,TRUE).'</pre>';
        if (is_array($value)) {
            $success = $value['Error'];
            unset($value['Error']);
            $value = serialize($value);
        }
        if ($type == 'RSXM') {
            $value = $this->RIJNDAEL256_encrypt($value);
            $value = base64_encode($value);
            $response = '<?xml version="1.0" ?>
					<open-realty_api>
					<open-realty_api_type>RSXM</open-realty_api_type>
					<open-realty_version>3.0</open-realty_version>
					<result_code>'.intval($success).'</result_code>
					<result>'.$value.'</result>
					</open-realty_api>';
        } else {
        }
        return $response;
    }

    /**
     * @param unknown_type $command
     * @param unknown_type $data
     * @param unknown_type $api_user
     * @param unknown_type $api_password
     * @return string
     * @access private
     */
    public function build_xml_query($command, $data, $api_user, $api_password)
    {
        $xmldata='';
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $xmldata .='<'.$key.'>'.serialize($value).'</'.$key.'>';
            }
        }
        //print_r($xmldata);
        //$data = serialize($xmldata);
        $data = $this->RIJNDAEL256_encrypt($xmldata);
        $data = base64_encode($data);
        //User
        $api_user = $this->RIJNDAEL256_encrypt($api_user);
        $api_user = base64_encode($api_user);
        //Pass
        $api_password = $this->RIJNDAEL256_encrypt($api_password);
        $api_password = base64_encode($api_password);
        $data = $this->RIJNDAEL256_encrypt($xmldata);
        $data = base64_encode($data);
        $command = serialize($command);
        $command = $this->RIJNDAEL256_encrypt($command);
        $command = base64_encode($command);
        $query = '<?xml version="1.0" ?>
				<open-realty_api>
				<open-realty_api_type>RSXM</open-realty_api_type>
				<request>
					<user>'.$api_user.'</user>
					<password>'.$api_password.'</password>
					<command>'.$command.'</command>
					<options>'.$data.'</options>

				</request>
				</open-realty_api>';
        return $query;
    }

    /**
     * This function is used to retrieve the soap responce from the API Server and unencrypt and unserialze it.
     *
     * @param string $value This is the encrpted string returned by the API server
     * @return array
     * @access private
     */
    public function retrieve_xml_query($value)
    {
        //echo '<pre>Raw Query Value: '.$value."</pre>";
        $value = simplexml_load_string($value);
        //echo '<pre>XML Query Value: '.print_r($value,TRUE).'</pre>';die;
        if ($value->{'open-realty_api_type'} =='RSXM') {
            $api_result_options = base64_decode($value->{'request'}->options);
            $api_result_options = $this->RIJNDAEL256_decrypt($api_result_options);
            //$api_result_options = unserialize($api_result_options);
            //print_r($api_result_options);
            $api_result_options = simplexml_load_string('<options>'.$api_result_options.'</options>');
            //print_r($api_result_options);
            $api_result_options = (array)$api_result_options;
            foreach ($api_result_options as $akey => $avalue) {
                $api_result_options[$akey]=unserialize($avalue);
            }
            //print_r($api_result_options);
            $api_result_command = base64_decode($value->{'request'}->command);
            $api_result_command = $this->RIJNDAEL256_decrypt($api_result_command);
            $api_result_command = unserialize($api_result_command);
            //Get User
            $api_result_user = base64_decode($value->{'request'}->user);
            $api_result_user = $this->RIJNDAEL256_decrypt($api_result_user);

            //Get Password
            $api_result_password = base64_decode($value->{'request'}->password);
            $api_result_password = $this->RIJNDAEL256_decrypt($api_result_password);
            //echo $api_result_command;
            $api_result_type = 'RSXM';
        } else {
            $api_result = base64_decode($value->{'request'});
            $api_result = unserialize($api_result);
        }

        $api_result=['command'=> $api_result_command,'options'=>$api_result_options,'type'=>$api_result_type,'user'=>$api_result_user,'password'=>$api_result_password];
        //echo '<pre>API Query Result: '.print_r($api_result,TRUE).'</pre>';
        return $api_result;
    }

    /**
     * @param unknown_type $value
     * @return multitype:unknown string
     * @access private
     */
    public function retrieve_xml_respose($value)
    {
        //echo '<pre>Raw Response Value: '.$value."</pre>";
        $value = simplexml_load_string($value);
        //  echo '<pre>XML Response Value: '.print_r($value,TRUE).'</pre>';
        if ($value->{'open-realty_api_type'} =='RSXM') {
            $api_or_version = (string)$value->{'open-realty_version'};
            //$api_or_version = $this->RIJNDAEL256_decrypt($api_or_version);
            //$api_result_options = unserialize($api_result_options);
            $api_result_code = (string)$value->{'result_code'};
            //$api_result_code = $this->RIJNDAEL256_decrypt($api_result_code);
            //$api_result_command = unserialize($api_result_command);
            $api_result = base64_decode($value->{'result'});
            $api_result = $this->RIJNDAEL256_decrypt($api_result);
            $api_result = unserialize($api_result);
        } else {
            $api_result = base64_decode($value->{'request'});
            $api_result = unserialize($api_result);
        }

        $api_result=['api_or_version'=> $api_or_version,'api_result_code'=>$api_result_code,'api_result'=>$api_result];
        //echo '<pre>API Response Result: '.print_r($api_result,TRUE).'</pre>';
        return $api_result;
    }

    /**
     * @param unknown_type $server
     * @param unknown_type $post
     * @return string|mixed
     */
    private function exec_curl($server, $post)
    {
        $link = curl_init();
        curl_setopt($link, CURLOPT_URL, $server);
        curl_setopt($link, CURLOPT_POST, 1);
        curl_setopt($link, CURLOPT_POSTFIELDS, $post);
        curl_setopt($link, CURLOPT_VERBOSE, 0);
        curl_setopt($link, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($link, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($link, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($link, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($link, CURLOPT_MAXREDIRS, 6);
        curl_setopt($link, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($link, CURLOPT_TIMEOUT, 60);
        $results = curl_exec($link);
        if (curl_errno($link) != 0) {
            curl_close($link);
            return false;
        }
        curl_close($link);
        return $results;
    }

    /**
     *  * This function is used to call remote api commands.
     *
     * @param string $api_server - URL of the Open-Realty install to run the api command on. Ex http://yourdomain.com/admin/index.php
     * @param string $api_command - Name of the api command to run. Example. pclass__create
     * @param array $api_data -  Array of data to pass to the API Command. See Each API Command for details on what to pass.
     * @param string $api_user - Username to run the api command as.
     * @param string $api_password - Password for teh user running api command as.
     * @return array
     */
    public function send_api_command($api_server, $api_command, $api_data, $api_user = '', $api_password = '')
    {
        $query = $this->build_xml_query($api_command, $api_data, $api_user, $api_password);
        //echo '<pre>Sending Query:'.$query."</pre>";
        $query=urlencode($query);
        $result = $this->exec_curl($api_server, "orapi_query=$query");
        //  echo '<pre>Raw CURL Result: '.print_r($result,TRUE)."</pre>";
        $result = $this->retrieve_xml_respose($result);
        //echo '<pre>Query Result: '.print_r($result,TRUE)."</pre>";
        return $result;
    }

    /**
        * api::load_remote_api()
        *
        * @param string $api_command This should contain the name of the api_command to execute
        * @param array $api_data This should contain an array or the variables to pass to the api_command.
        * @return string
        * @access private
        *
        **/
    public function load_remote_api($query)
    {
        global $lang, $lapi, $config;

        $query = $this->retrieve_xml_query($query);

        //Decrypt Data
        //$d_api_user = $this->RIJNDAEL256_decrypt(base64_decode($api_user));
        //$d_api_pass = $this->RIJNDAEL256_decrypt(base64_decode($api_pass));
        //$d_api_admin = $this->RIJNDAEL256_decrypt(base64_decode($api_admin));
        //
        $api_command = $query['command'];
        $api_data = $query['options'];
        $api_type = $query['type'];
        $api_user = $query['user'];
        $api_password = $query['password'];

        //Log User In
        $_POST['user_name'] = $api_user;
        $_POST['user_pass'] = $api_password;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $login_status = $login->loginCheck('Agent', true);

        if ($login_status !== true) {
            $result = ['0',['error_code' => 10001, 'error_msg' => 'Login Failed']];
        } else {
            // Check that api_data is an array
            if (!is_array($api_data)) {
                $result = ['0',['error_code' => 4001, 'error_msg' => $lang['error_value_not_array']]];
            //Log error Next
            } else {
                //extract($api_data, EXTR_SKIP || EXTR_REFS, '');
                $api_command = explode('__', $api_command);
                if (BETA_API) {
                    include_once dirname(__FILE__).'/beta-commands/'.$api_command[0].'.inc.php';
                } else {
                    include_once dirname(__FILE__).'/commands/'.$api_command[0].'.inc.php';
                }

                $apiclass=$api_command[0].'_api';
                $api_sub = new $apiclass();
                $result = $api_sub->{$api_command[1]}($api_data);
            }
        }
        $result = $this->build_xml_response($result, $api_type);
        return $result;
    }

    /**
     * This function is used to call local api commands.
     *
     * @param string $api_command - Name of the api command to run. Example. pclass__create
     * @param array $api_data - REQUIRED - Array of data to pass to the API Command. See Each API Command for details on what to pass.
     * @param string $api_user - OPTIONAL username to run the api command as. This is only required if called from outside of Open-Realty.
     * @param string $api_password - OPTIONAL password for the user running api command as. This is only required if called from outside of Open-Realty.
     *
     * @return array
     */
    public function load_local_api($api_command, $api_data, $api_user = '', $api_password = '')
    {
        global $lang,$lapi,$config;

        if ($api_user!='') {
            $_POST['user_name'] = $api_user;
            $_POST['user_pass'] = $api_password;
            include_once $config['basepath'] . '/include/login.inc.php';
            $login = new login();
            $login_status = $login->loginCheck('Agent', true);
            if ($login_status !== true) {
                return ['error' => true,'error_msg'=>'Login Failed'];
            }
        }

        //echo '<pre>Lang: '.print_r($lang,TRUE).'</pre>';
        // Check that api_data is an array
        if (!is_array($api_data)) {
            return [false,$lang['error_value_not_array'],$api_data];
        }
        //Extract the api data passed via SOAP into individual variables. These new variables are actually just references to save on memory
        //extract($api_data, EXTR_SKIP || EXTR_REFS, '');
        $api_command = explode('__', $api_command);
        if (preg_match('/[^A-Za-z0-9]/', $api_command[0])) {
            //File name contains non alphanum chars die to prevent file system attacks.
            die('Invalid API Call - File Attack');
        }

        if (BETA_API) {
            if (!file_exists(dirname(__FILE__).'/beta-commands/'.$api_command[0].'.inc.php')) {
                die('Invalid API Call');
            }
            include_once dirname(__FILE__).'/beta-commands/'.$api_command[0].'.inc.php';
        } else {
            if (!file_exists(dirname(__FILE__).'/commands/'.$api_command[0].'.inc.php')) {
                die('Invalid API Call');
            }
            include_once dirname(__FILE__).'/commands/'.$api_command[0].'.inc.php';
        }
        //require_once(dirname(__FILE__).'/commands/'.$api_command[0].'.inc.php');
        $this->load_command_lang($api_command[0]);
        $apiclass=$api_command[0].'_api';
        $api_sub = new $apiclass();
        $api_data = $this->scrub_api_data($api_data);
        $result = $api_sub->{$api_command[1]}($api_data);
        return $result;
    }

    /**
     * This makes sure the user has not tried to override any global variables in the API data.
     *
     * @param array $data This is the api data to scrub
     * @return array
     * @access private
     */
    public function scrub_api_data($data)
    {
        unset($data['conn']);
        unset($data['lang']);
        unset($data['config']);
        unset($data['db_type']);
        unset($data['api']);
        unset($data['lapi']);
        unset($data['misc']);
        return $data;
    }

    /**
     * This Creates the $db connection for the API and also creates the $settings and $lang variables.
     *
     * @access private
     */
    public function load_setting()
    {
        global $config,$lang;
        //Load Config Look first for OR's common.php file then fall back to a common.php in this folder for remote usage
        if (file_exists(dirname(__FILE__).'/../include/common.php')) {
            include_once dirname(__FILE__).'/../include/common.php';
        } elseif (file_exists(dirname(__FILE__).'/common.php')) {
            include_once dirname(__FILE__).'/common.php';
        }

        /*
        // Load the default API Language File
        if(file_exists(dirname(__FILE__).'/../lang/'.$config['lang'].'/api.inc.php')){
            include_once dirname(__FILE__).'/../lang/'.$config['lang'].'/api.inc.php';
        }
        */
    }

    /**
     * This function loads any languages files needed by the API Command. This is mainly used for error handling.
     *
     * @param string $command Name of the command set being run.
     * @access private
     */
    public function load_command_lang($command)
    {
        // global $config;
        // if (file_exists(dirname(__FILE__).'/../lang/'.$config['lang'].'/'.$command.'.inc.php')) {
        //     include_once dirname(__FILE__).'/../lang/'.$config['lang'].'/'.$command.'.inc.php';
        // }
    }
}
