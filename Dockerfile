FROM registry.gitlab.com/appsbytherealryanbonham/open-realty-docker:0.0.9
# RUN adduser deploy
RUN chmod -R 777 /tmp

COPY ./ /tmp/open-realty/

WORKDIR /tmp/open-realty/

RUN yarn install

# Install composer dependencies
RUN  wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
RUN  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN  php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN  php composer-setup.php
RUN  php -r "unlink('composer-setup.php'); unlink('installer.sig');"

RUN php composer.phar install --no-dev 

RUN mv /tmp/open-realty/src/* /var/www/html/
RUN touch /var/www/html/devignoreinstall
RUN rm -rf /tmp/open-realty/
EXPOSE 80
