# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [#.#.#] - Unreleased

## [3.5.4] - Unreleased

- Fixed Some E_NOTICE errors with Contact Form
- Fix some e_warning errors with the view lead pagination
- Fix an error in login code for storing user data in sessions
- Try to ensure we are running E_ALL error reporting if devignoreinstall is present
- Update Build images to use latest PHP 8 docker image with development-ini enabled.
- Fix warning errors that were breaking ability to edit media in media widget.

### Fixed

## [3.5.3] - 2021-12-30

More security improvements and bug fixes.

### Fixed

- SECURITY: Fix File Path Traversal in Filemanager jqueryFileTree.php
- DEPRECATED: Ability to enable execution of PHP Code in WYSIWYG editor. This feature is BAD idea and is going away in 3.6.0. Use Addons instead of writing PHP in the WYSIWYG pages/blogs.
- Delete Item on menu editor was not working.
- Minor JS cleanups.
- Add CSRF Tokens to agent & member signup forms
- Added Xdebug config to docker-compose to support local debugging.
- Add CSRF Tokens to login forms.
- Added CSRF Tokens to Template Switcher
- Template switcher no longer support GET vars you must use POST
- Mobile Template Changer `{mobile_full_template_link}` only shows if allow_template_change is enabled.
- Add CSRF Tokens to contact agent form.
- Cleanup contact agent form code, and remove pro & free templates and replace with new default template.
- Fix SQL Errors in replace_search_field_tags()
- Fix JS for Flex, Lazuli, and Mobile Template
- Code review for phpcs warnings, and setting ignores where safe.
- Add CSRF Tokens to save search form
- Fix handling of duplicate saved searches
- Fix broken link when duplicate saved searches occur.

### Template Changes

- admin/template/default/login.html
- admin/template/default/menu_editor_index.html
- template/default/agent_signup.html
- template/default/contact_agent_default.html (Added)
- template/default/contact_agent_free.html (Deleted)
- template/default/contact_agent_pro.html (Deleted)
- template/default/login.html
- template/default/member_signup.html
- template/default/saved_searches_add.html
- template/flex.main.html
- template/html5/featured_listing_horizontal.html
- template/html5/listing_detail_default.html
- template/lazuli/main.html
- template/mobile/main.html
- template/mobile/page_1.html
-

### Language Changes

- Removed `$lang["incorrect_password"]`
- Removed `$lang["nonexistent_username"]`
- Updated `$lang["wysiwyg_execute_php_desc"]`
- Updated `$lang["allow_template_change_desc"]`
- Added `$lang["invalid_csrf_token"]`
- Added `$lang["signup_already_logged_in"]`
- Added `$lang["incorrect_username_password"]`

## [3.5.2] - 2021-12-22

After hearing from some user that the upgrade to PHP 8 was not possible currently on there hosting providers, we have changed the minimum requirements back down to php >7.4.3. There are no other changes in this release.

### Changed

- Rebuild for PHP > 7.4.3
- Update installer to check for php 7.4.3 instead of 8.0 when checking requirements.

## [3.5.1] - 2021-12-19

Some important bug fixes..

### Fixed

- Blog & Page Editors Not Loading
- Improvement to pingback registration, including fixed bug where registration failed if you were not using SEO Urls
- Fix MagicParser for SEO URls
- Fix a bunch of code bugs found via intellisense.
- Update phpxmlrpc to v4.
- Upgrade browscap-php to v6
- Update Timezone list

## [3.5.0] - 2021-12-15

The focus on 3.5.0 is updating the open-realty jquery and other dependencies, improving security, and bug fixes. All users are strongly recommended to upgrade. Please note that third party templates may need to be upgraded/adjusted as part of this due to the dependency upgrades.

### Added

- Improved CI Builds to start running unit test & code coverage.

### Changed

- PHP Libarary Changes
  - Replace aferrandini/phpqrcode library endroid/qr-code library and phpqrcode was no longer maintained.
  - Replaced dapphp/securimage with gregwar/captcha, as dapphp/securimage was no longer maintained.

### Fixed

- CkEditor Filemanager
  - Fix bug in root folder protection
  - Fix Generic Object Injection Sink warnings
- Log Viewer
  - Remove Dead Code
  - Fix Generic Object Injection Sink warnings

## [3.5.0-beta.1] - 2021-12-04

The focus on 3.5.0 is updating the open-realty jquery and other dependencies, improving security, and bug fixes. All users are strongly recommended to upgrade. Please note that third party templates may need to be upgraded/adjusted as part of this due to the dependency upgrades.

### Fixed

- SECUIRTY PATCH: Fixed a file exposure bug in CKEditor Filemanager plugin.
- There were a number of other security bugs fixed in the js library upgrades etc..
- A number of possible undefined var errors
- Cookies are not http only, improving security
- Enabled Security Scanning for CI
- Fixed menu editor dropdown for Blog / Page selection not working
- Fix colorbox that broke with latest jquery version.
- Update php-cs-fixer to format the CKEditor filemanager plugin
- Removed Old Jquery Versions
  - Remove Jquery 1.7.1
  - Remove jquery-1.2.6 from CKEditor filemanager plugin
- Removed JQuery Tools - This Change the Slideshow, Featured Listings, and Listing Statistics templates.
- Moved CKEditor installation from composer to yarn.
- Moved Third Party JS Packages to YARN and updated versions
  - superfish.js
  - jquery.uniform
  - ckeditor-youtube-plugin
  - ckeditor-quicktable-plugin
  - ckeditor4
  - jquery.splitter
  - jqueryfiletree
  - jstree
  - tablesorter
  - jquery.impromptu
  - jquery-filedrop
  - lightSlider
- Removed js-tree package that was not being used.
- Update CKEditor Plugins
  - YouTube
  - TableResizer
  - QuickTable
  - Open-Realty Filemanager

### Template Changes

- admin/template/OR_small/main.html
- admin/template/default/addon_manager.html
- admin/template/default/blog_edit_index.html
- admin/template/default/listing_template_editor.css
- admin/template/default/menu_editor.css
- admin/template/default/menu_editor_index.html
- admin/template/default/site_config_general.html
- template/default/listing_detail_slideshow.html
- template/default/saved_searches.html
- template/default/style_default.css
- template/html5/admin_bar.css
- template/html5/featured_listing_horizontal.html
- template/html5/lib/MIT-LICENSE.txt (DELETED)
- template/html5/lib/jquery.uniform.min.js (DELETED)
- template/html5/lib/superfish.js (DELETED)
- template/html5/listing_detail_default.html
- template/html5/main.html
- template/html5/popup.html
- template/html5/style.css
- template/lazuli/main.html
- template/lazuli/popup.html
- template/mobile/main.html
- template/mobile/page1_main.html

## [3.4.3] - 2021-11-25

Security Fix for moment.js, vcard support, more PHP8 fixes

### Fixed

- Agent vcard download reenabled
- moment.js - SECURITY FIX
- Fix Blog Count
- PHP 8 / ADODB Fixes.

## Changed

- Javascript Library Update
  - Jquery 3.6
  - moment.js
  - fullcandar v3
- Updated php-cs-fixer
- Updated docker-compose.yml to support testing mysql8 and mariadb 10.7

## Changed

## [3.4.2] - 2021-11-21

Initial Support For PHP 8.

### Fixed

- Fixed numerous PHP 8 warnings
- Fixed Banned IP/Domain Detection on PHP 8
- Fixed Bug in Listing Field Create that required a Search Type Set
- Many other small bug fixes related to the PHP Warning Fixes.

### Template Changes

- admin/template/default/site_config_users.html Changed {lang_agent_default_canChangeExpirations_desc} to {lang_agent_default_canchangeexpirations_desc}
- admin/template/default/view_logs.html Changed {lang_log_viewer\_} to {lang_log_viewer}

## [3.4.1] - 2021-03-04

### Changed

- [#15](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/15) get_latest_releases now caches response from server.
- [#16](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/16) Improve Logging when get_url calls fail
- JQuery Form upgraded and now installed via yarn.

### Fixed

- [#14](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/14) Autoupgrade was not correctly running db upgrades and bumping version number.
- [#17](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/17) Do not override .htaccess during upgrade.

### Template Changes

- admin/template/default/blog_edit_index.html Javascript for Blog Index now part of template.

## [3.4.0] - 2021-02-21

Our first stable release under the new MIT license. Only changes since beta 4 are included in this changelog, see beta changelogs for full set of changes.

### Fixed

- Removed browser debug output on page, if tracking is enabled.
- Fix broken div on pages with securimage captchas.
- No longer show "default" template folder for template selection in site config.
- No longer show property class specific template for listing template selection in site config.

### Template Changes

## [3.4.0-beta.4] - 2021-02-19

Here we write upgrading notes for open-realty. It's a team effort to make them as
straightforward as possible.

### Added

- [#6](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/6) Docs updated to include directions for [updating browsercap_cache](https://docs.open-realty.org/nav.guide/04.automating_tasks/browsercap_cache.html)

### Fixed

- Site Statistics recording fixed.
- [#11](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/11) Removed calls to deprecated get_magic_quotes_gpc()
- [#12](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/12) Missing /files/browsercap_cache folder added to package.
- [#10](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/10) Installer was checking for an old version of PHP updated to PHP 7.4.0 > as per current requirements.
- [#13](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/13) Addon Manager now allows easy download of addons.

### Template Changes

- admin/template/default/popup.html - Fixed CSS call to use load_css to ensure we load from default it css does not exist in template.

## [3.4.0-beta.3] - 2021-02-13

### Added

- Add docs to readme on how to run from source.

### Changed

- Improved CI performance.
- Removed Dockerfile, we now have a prebuilt docker image to save compile time on CI runs and local development.
- Added Test to CI to ensure version number is set correctly when we tag a new release.
- Updated README.md with correct PHP version requirements.
- Upgrade a notice now shows version you will upgrade to.

### Fixed

- Installer had an error when inserting data into userdb, due to missing field in insert.
- Improved RSS Feed compatibility with Readers. We comply to Atom Spec now. For existing users check your RSS Descriptions in the Social tab in site config. Replace any use of `{image_thumb_1}` with `{image_thumb_fullurl_1}` to improve compatibility.
- Auto Upgrade Feature now works for open source releases, using Gitlab API.

### Template Changes

- templates/default/rss.html

## [3.4.0-beta.2] - 2021-02-11

### Fixed

- [#1](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/1) Addon and Files folder were missing from install package
- [#2](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/2) common.dist.php file was missing
- Vendor Path was set incorrectly in common.php durring install.
- Fix api offset null error error
- Fix $display not set error in listing display.

## [3.4.0-beta.1] - 2021-02-08

### Added

- PHP 7.4 support
- We now use composer to install php dependencies.
- We now use yarn to install javascript dependencies. (80% Migrated)
- We use https://www.mortgagecalculator.org/ for our mortgage calculator
- Large code cleanup effort, much more still needed.

### Changed

- Project is now released under MIT license.
- Project is housed on Gitlab
- Update ADODB
- Update reCaptcha
- Updated Securimage
- Updated ckeditor
- Updated phpMailer
- Update DataTables
- Update Twitter Auth
- Update Jquery for Admin
- Update Jquery UI
- Updated twitteroauth
- Updated colorbox
- Updated jquery.cookie

### Fixed

- Fixed a bug in the user API update method that unnecessarily updated the Activity log when an unauthenticated user attempted to use it.
- Fixed a bug that prevented the listing ID from being passed to the Listing Detail page's printer friendly link, email to a friend link, and the add favorites link
- When the user functions were refactored for v3.3 we inadvertently broke the ability for the admin to complete a lost password reset operation
- The jQuery form validation would not allow an empty field type: Date if the field was not set to be required.
- OR v3.3 upgrade functions would not upgrade the DB when upgrading from v3.2.7
- The slideshow template's block tags were not being properly cleaned-up if a listing had no photos.
- get_featured_raw template tag was inadvertently disabled due to recent refactoring
- Agent signup was not always sending signup notifications to the Admin and to the person signing up when account moderation was active this was a side-effect of recent PDO refactoring.
- Improved error_reporting for the Field API and PHPmail function.
- Improved the lost password reset email validation check
- Removed deprecated agent_login_link tag from agent_activation_email.html. replaced with baseurl/admin
- Fixed the listing hit counter. This was broken for unauthenticated site visitors due to code refactoring in OR v3.3
- Update Media handling to deal with Protocol-relative URLs \
- Fixed session handling for Securimage
- Fixed extra db connection used by Adodb sessions
- Fixed Agent hit counter that was broken due to prior refactoring
- Fixed email validation that did not handle some email addresses correctly.
- Removed deprecated calculator and javascript library
